package com.thales.github.utilities;

public final class Constants {
    public static final String GITHUB_API_URL = "https://api.github.com/";

    private Constants() {

    }
}
